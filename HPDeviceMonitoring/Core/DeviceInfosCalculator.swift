//
//  DeviceInfosCalculator.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 08/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import Foundation
import UIKit

// Compute CPU and RAM used
class DeviceInfosCalculator {
    
    // MARK: - Publics computed properties
    
    var batteryUsedPercent: Float? {
        guard UIDevice.current.batteryState != .unknown else { return nil }
        return UIDevice.current.batteryLevel * 100
    }
    
    var cpuUsedPercent: Float? {
        guard let cpuInfo = hostCPULoadInfo() else { return nil }
        return Float(cpuInfo.sys) + Float(cpuInfo.usr)
    }
    
    var memoryUsedPercent: Float? {
        guard let ramInfo = memoryInfo() else { return nil }
        return Float((ramInfo.used * 100) / ramInfo.total)
    }

    // MARK: - Private properties

    // Used to calculate CPU load
    private var loadPrevious: host_cpu_load_info?
}

// MARK: - Internal computation for CPU
extension DeviceInfosCalculator {
    
    private struct CpuInfo {
        let sys: Double
        let usr: Double
        let idle: Double
        let nice: Double
    }
    
    private func hostCPULoadInfo() -> CpuInfo? {
        
        let HOST_CPU_LOAD_INFO_COUNT = MemoryLayout<host_cpu_load_info>.stride/MemoryLayout<integer_t>.stride
        var size = mach_msg_type_number_t(HOST_CPU_LOAD_INFO_COUNT)
        var load = host_cpu_load_info()
        
        guard let loadPrevious = self.loadPrevious else {
            self.loadPrevious = load
            return nil
        }
        
        let result = withUnsafeMutablePointer(to: &load) {
            $0.withMemoryRebound(to: integer_t.self, capacity: HOST_CPU_LOAD_INFO_COUNT) {
                host_statistics(mach_host_self(), HOST_CPU_LOAD_INFO, $0, &size)
            }
        }
        if result != KERN_SUCCESS{
            print("Error  - \(#file): \(#function) - kern_result_t = \(result)")
            return nil
        }
        
        let usrDiff: Double = Double(load.cpu_ticks.0 - loadPrevious.cpu_ticks.0);
        let systDiff = Double(load.cpu_ticks.1 - loadPrevious.cpu_ticks.1);
        let idleDiff = Double(load.cpu_ticks.2 - loadPrevious.cpu_ticks.2);
        let niceDiff = Double(load.cpu_ticks.3 - loadPrevious.cpu_ticks.3);
        
        let totalTicks = usrDiff + systDiff + idleDiff + niceDiff
        
        let sys = systDiff / totalTicks * 100.0
        let usr = usrDiff / totalTicks * 100.0
        let idle = idleDiff / totalTicks * 100.0
        let nice = niceDiff / totalTicks * 100.0
        
        let cpuInfo = CpuInfo(sys: sys, usr: usr, idle: idle, nice: nice)
        
        self.loadPrevious = load
        
        return cpuInfo
    }
}

// MARK: - Internal computation for Memory
extension DeviceInfosCalculator {
    
    typealias MemoryInfo = (used: UInt64, total: UInt64)
    
    // TODO: not confident of this computation... It's seams not works well
    // I have loooking for this topic on internet, but not found a good solution :-(
    private func memoryInfo() -> MemoryInfo? {
        
        var taskInfo = mach_task_basic_info()
        var count = mach_msg_type_number_t(MemoryLayout<mach_task_basic_info>.size)/4
        let result: kern_return_t = withUnsafeMutablePointer(to: &taskInfo) {
            $0.withMemoryRebound(to: integer_t.self, capacity: 1) {
                task_info(mach_task_self_, task_flavor_t(MACH_TASK_BASIC_INFO), $0, &count)
            }
        }
        
        guard result == KERN_SUCCESS else { return nil }
        
        let used = UInt64(taskInfo.resident_size)
        let total = ProcessInfo.processInfo.physicalMemory
        
        return (used, total)
    }
    
    
    // MARK: - Other try to compute Memory ... but not seams better
    
    //    func mach_task_self() -> task_t {
    //        return mach_task_self_
    //    }
    //
    //    func getMegabytesUsed() -> Float? {
    //        var info = mach_task_basic_info()
    //        var count = mach_msg_type_number_t(MemoryLayout.size(ofValue: info) / MemoryLayout<integer_t>.size)
    //        let kerr = withUnsafeMutablePointer(to: &info) { infoPtr in
    //            return infoPtr.withMemoryRebound(to: integer_t.self, capacity: Int(count)) { (machPtr: UnsafeMutablePointer<integer_t>) in
    //                return task_info(
    //                    mach_task_self(),
    //                    task_flavor_t(MACH_TASK_BASIC_INFO),
    //                    machPtr,
    //                    &count
    //                )
    //            }
    //        }
    //        guard kerr == KERN_SUCCESS else {
    //            return nil
    //        }
    //        return Float(info.resident_size) / (1024 * 1024)
    //    }
    //
    //    func getMemoryV3() -> Float? {
    //
    //        let TASK_VM_INFO_COUNT = MemoryLayout<task_vm_info_data_t>.size / MemoryLayout<natural_t>.size
    //
    //        var vmInfo = task_vm_info_data_t()
    //        var vmInfoSize = mach_msg_type_number_t(TASK_VM_INFO_COUNT)
    //
    //        let kern: kern_return_t = withUnsafeMutablePointer(to: &vmInfo) {
    //            $0.withMemoryRebound(to: integer_t.self, capacity: 1) {
    //                task_info(mach_task_self_,
    //                          task_flavor_t(TASK_VM_INFO),
    //                          $0,
    //                          &vmInfoSize)
    //            }
    //        }
    //
    //        guard kern == KERN_SUCCESS else {
    //            return nil
    //        }
    //
    ////        let usedSize = DataSize(bytes: Int(vmInfo.internal + vmInfo.compressed))
    ////        print("Memory in use (in bytes): %u", usedSize)
    ////
    ////        return usedSize
    //
    //        let totalUsed = vmInfo.internal + vmInfo.compressed
    //
    //        print("Memory in use (in bytes): \(totalUsed)")
    //
    //        return nil
    //    }

}
