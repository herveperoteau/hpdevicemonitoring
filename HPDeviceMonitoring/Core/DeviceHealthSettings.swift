//
//  DeviceHealthSettings.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 05/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import Foundation

// MARK: - DeviceHealthSettings: Protocol definition
protocol DeviceHealthSettings: CustomStringConvertible {
    func thresholdPercent(instrument: Instrument) -> Float
    func isNotifiedForRecovered(instrument: Instrument) -> Bool
}

// MARK: - DeviceHealthBundleSettings: implementation of DeviceHealthSettings Protocol
final class DeviceHealthBundleSettings: DeviceHealthSettings {

    func thresholdPercent(instrument: Instrument) -> Float {
        switch instrument {
        case .battery: return batteryThreshold
        case .cpu: return cpuThreshold
        case .ram: return ramThreshold
        }
    }
    
    func isNotifiedForRecovered(instrument: Instrument) -> Bool {
        switch instrument {
        case .battery: return isNotifiedForBatteryRecovered
        case .cpu: return isNotifiedForCpuRecovered
        case .ram: return isNotifiedForRamRecovered
        }
    }
    
    var description: String {
        return "batteryThreshold:\(batteryThreshold) [recovered:\(isNotifiedForBatteryRecovered)], cpuThreshold:\(cpuThreshold) [recovered:\(isNotifiedForCpuRecovered)], ramThreshold:\(ramThreshold) [recovered:\(isNotifiedForRamRecovered)]"
    }

    var batteryThreshold : Float {
        return UserDefaults.standard.float(forKey: BundleKeys.batteryThreshold)
    }

    var cpuThreshold : Float {
        return UserDefaults.standard.float(forKey: BundleKeys.cpuThreshold)
    }

    var ramThreshold : Float {
        return UserDefaults.standard.float(forKey: BundleKeys.ramThreshold)
    }
    
    var isNotifiedForBatteryRecovered : Bool {
        return UserDefaults.standard.bool(forKey: BundleKeys.batteryNotifiedWhenRecovered)
    }

    var isNotifiedForCpuRecovered : Bool {
        return UserDefaults.standard.bool(forKey: BundleKeys.cpuNotifiedWhenRecovered)
    }
    
    var isNotifiedForRamRecovered : Bool {
        return UserDefaults.standard.bool(forKey: BundleKeys.ramNotifiedWhenRecovered)
    }
    
    init() {
        registerDefaultsValuesIfNeeded()
    }

    // MARK: - Private properties and methods
    
    private struct Defaults {
        static let batteryThreshold = 50
        static let cpuThreshold = 40
        static let ramThreshold = 30
        static let batteryNotifiedWhenRecovered = true
        static let cpuNotifiedWhenRecovered = true
        static let ramNotifiedWhenRecovered = true
    }
    
    private struct BundleKeys {
        static let batteryThreshold = "battery_threshold"
        static let batteryNotifiedWhenRecovered = "battery_notify_when_recovered"
        static let cpuThreshold = "cpu_threshold"
        static let cpuNotifiedWhenRecovered = "cpu_notify_when_recovered"
        static let ramThreshold = "ram_threshold"
        static let ramNotifiedWhenRecovered = "ram_notify_when_recovered"
    }
    
    private func registerDefaultsValuesIfNeeded() {
        if let _ = UserDefaults.standard.object(forKey: BundleKeys.batteryThreshold) {
            // Already setup
            return
        }
        
        UserDefaults.standard.register(
            defaults: [
                BundleKeys.batteryThreshold: Defaults.batteryThreshold,
                BundleKeys.cpuThreshold: Defaults.cpuThreshold,
                BundleKeys.ramThreshold: Defaults.ramThreshold,
                BundleKeys.batteryNotifiedWhenRecovered: Defaults.batteryNotifiedWhenRecovered,
                BundleKeys.cpuNotifiedWhenRecovered: Defaults.cpuNotifiedWhenRecovered,
                BundleKeys.ramNotifiedWhenRecovered: Defaults.ramNotifiedWhenRecovered,
                ])
    }
}
