//
//  PersistenceService.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 05/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import Foundation

protocol PersistenceService {
    
    func saveMeasures(_ measures: [Measure]) throws
    func getMeasuresHistory(instrument: Instrument, limit: Int) throws -> [Measure]
    
    func saveNotifications(_ notifs: [Notification]) throws
    func getNotificationsHistory(limit: Int) throws -> [Notification]

    func deleteRecords(before: Date) throws
}
