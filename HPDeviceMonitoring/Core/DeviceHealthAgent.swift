//
//  DeviceHealthAgent.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 05/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

typealias DeviceHealthAgentListener = (_ measures: [Measure]) -> Void

class DeviceHealthAgent {
    
    struct Const {
        static let refreshTimeInterval : TimeInterval = 1.0
        
        static let keyCurrentStateUserDefault = "HPDeviceHealthCurrentState_"
    }
    
    private let settings: DeviceHealthSettings
    private let persistenceService: PersistenceService
    
    private var lastMeasures: [Instrument: Measure] = [:]
    
    private var currentStates: [Instrument: AlarmStatus] = [:]
    
    private var listeners: [String: DeviceHealthAgentListener] = [:]
    
    lazy var listenersQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "listenersQueue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    private var refreshTimer: Timer?
    
    private let calculator = DeviceInfosCalculator()
    
    // Background mode
    var backgroundTask: UIBackgroundTaskIdentifier = .invalid

    // TODO: to be removed : Stuffs for Simul Fake Activity
    public var simulFakeActivity = false
    private var internalCounter = 0
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    init(settings: DeviceHealthSettings, persistenceService: PersistenceService) {

        UIDevice.current.isBatteryMonitoringEnabled = true
        
        self.settings = settings
        print(settings)
        
        self.persistenceService = persistenceService
        
        self.loadCurrentStates()
    }
    
    func start() {
        
        registerForNotifications()
        registerBackgroundTask()

        // Battery refresh is handle by batteryLevelDidChangeNotification
        refreshMeasureBattery()

        // Others measures are get in a loop
        startLoopCaptureMeasures()
    }
        
    private func registerForNotifications() {
        
        NotificationCenter.default.addObserver(
            forName: UIApplication.didBecomeActiveNotification,
            object: nil,
            queue: nil) { (notif) in
                self.registerBackgroundTask()
                print(self.settings)
        }
        
        // Battery watcher
        NotificationCenter.default.addObserver(
            forName: UIDevice.batteryLevelDidChangeNotification,
            object: nil,
            queue: nil) { (notif) in
                self.refreshMeasureBattery()
        }
    }
    
    // MARK: - Loop capture measures
    private func startLoopCaptureMeasures() {
        
        refreshTimer?.invalidate()
        refreshTimer = Timer.scheduledTimer(withTimeInterval:Const.refreshTimeInterval, repeats: true, block: { [weak self] (timer) in
            guard let `self` = self else { return }
            if self.simulFakeActivity {
                self.refreshFakeMeasures()
            } else {
                self.refreshMeasuresPerformance()
            }
        })
    }
    
    private func refreshFakeMeasures() {
        
        internalCounter += 1
        
        let cpu: Float = Float((self.internalCounter+80) % 100)
        let battery: Float = Float((self.internalCounter+20) % 100)
        let ram: Float = Float((self.internalCounter+60) % 100)

        let ramMeasure = MeasureImpl(date: Date(), instrument: .ram, percentValue: ram)
        let cpuMeasure = MeasureImpl(date: Date(), instrument: .cpu, percentValue: cpu)
        let batteryMeasure = MeasureImpl(date: Date(), instrument: .battery, percentValue: battery)

        lastMeasures[.ram] = ramMeasure
        lastMeasures[.cpu] = cpuMeasure
        lastMeasures[.battery] = batteryMeasure

        handleMeasures(measures:[ramMeasure, cpuMeasure, batteryMeasure])
    }

    private func refreshMeasuresPerformance() {
        
        var measures: [Measure] = []

        let cpu = calculator.cpuUsedPercent
        let memory = calculator.memoryUsedPercent
        
        if let cpu = cpu {
            let cpuMeasure = MeasureImpl(date: Date(), instrument: .cpu, percentValue: cpu)
            measures.append(cpuMeasure)
            lastMeasures[.cpu] = cpuMeasure
        }
        
        if let memory = memory {
            let memoryMeasure = MeasureImpl(date: Date(), instrument: .ram, percentValue: memory)
            measures.append(memoryMeasure)
            lastMeasures[.ram] = memoryMeasure
        }

        handleMeasures(measures:measures)
    }

    // Used by background fetch or when Battery notification received
    func refreshMeasureBattery() {
        
        if self.simulFakeActivity {
            refreshFakeMeasures()
            return
        }
        
        guard let batteryPercent = self.calculator.batteryUsedPercent else {
            return
        }
            
        let batteryMeasure = MeasureImpl(date: Date(), instrument: .battery, percentValue: batteryPercent)
        lastMeasures[.battery] = batteryMeasure
        
        handleMeasures(measures: [batteryMeasure])
    }
    
    private func handleMeasures(measures: [Measure]) {
        
        guard measures.count > 0 else { return }
        
        // Check Alarm
        measures.forEach { checkForNotification(measure: $0) }
        
        // Dispatch persistence
        persistMeasures(measures)
        
        // Dispatch to listeners if app is active
        let applicationState = UIApplication.shared.applicationState
        if applicationState == .active {
            dispatchToListeners(measures: measures)
        }
    }

}

// MARK:- Notifications Alarm or Recovered
extension DeviceHealthAgent {
    
    private func checkForNotification(measure: Measure) {
        
        let thresholdPercent = settings.thresholdPercent(instrument: measure.instrument)
        
        var alarmType: AlarmStatus = .recovered
        
        if measure.percentValue >= thresholdPercent {
            if measure.instrument == .battery {
                alarmType = .recovered
            } else {
                alarmType = .alarm
            }
        } else {
            if measure.instrument == .battery {
                alarmType = .alarm
            } else {
                alarmType = .recovered
            }
        }
        
        // Check if state alarm updated
        let previousState = self.currentStates[measure.instrument]
        
        // TODO remove it, this is to test if background fetch works... seams that not :-(
        let applicationState = UIApplication.shared.applicationState
        let testBackoundFetch = applicationState == .background && measure.instrument == .battery && !self.simulFakeActivity
        
        if alarmType == previousState && !testBackoundFetch {
            return
        }
        
        self.currentStates[measure.instrument] = alarmType
        self.saveCurrentState(instrument: measure.instrument, state: alarmType)
        
        if alarmType == .recovered {
            
            if !settings.isNotifiedForRecovered(instrument: measure.instrument) {
                // No notif for recovered (conform to user settings)
                return
            }
            
            if previousState == nil {
                // No alarm in progress (no necessary to send a recovered alarm)
                return
            }
        }
        
        // Send a new Notification
        let notif = NotificationImpl(date: measure.date,
                                     instrument: measure.instrument,
                                     percentValue: measure.percentValue,
                                     typeNotification: alarmType)
        
        persistNotification(notif)
        sendNotification(notif)
    }
    
    private func sendNotification(_ notif: Notification) {
        print("sendNotification \(notif.textNotification)")
        let message = notif.textNotification
        let title = notif.typeNotification.description
        let identifier = "\(notif.typeNotification.description)-\(notif.instrument.description)"
        scheduleNotification(identifier:identifier, title: title, message: message)
    }
    
    private func scheduleNotification(identifier: String, title: String, message: String) {
        
        let content = UNMutableNotificationContent()
        
        content.title = title
        content.body = message
        content.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
        
        let identifier = identifier
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request)
    }
}

// MARK:- Persistence current state in UserDefault
extension DeviceHealthAgent {
    
    private func loadCurrentStates() {
        for instrument in Instrument.allCases {
            if let status = loadCurrentState(instrument: instrument) {
                self.currentStates[instrument] = status
            }
        }
    }
    
    private func keyUserDefault(instrument: Instrument) -> String {
        return "\(Const.keyCurrentStateUserDefault)_\(instrument.rawValue)"
    }
    
    private func saveCurrentState(instrument: Instrument, state: AlarmStatus) {
        UserDefaults.standard.set(state.rawValue, forKey: keyUserDefault(instrument: instrument))
    }
    
    private func loadCurrentState(instrument: Instrument) -> AlarmStatus? {
        guard
            let statusValue = UserDefaults.standard.object(forKey: keyUserDefault(instrument: instrument)) as? Int32,
            let status = AlarmStatus(rawValue: statusValue)
            else {
                return nil
        }
        
        return status
    }
}

// MARK:- Persistence measures in CoreData
extension DeviceHealthAgent {
    
    private func persistMeasures(_ measures: [Measure]) {
        do {
            try self.persistenceService.saveMeasures(measures)
        } catch {
            print("Error save measures")
        }
    }
    
    private func persistNotification(_ notif: Notification) {
        do {
            try self.persistenceService.saveNotifications([notif])
        } catch {
            print("Error save notification \(notif.textNotification)")
        }
    }
}

// MARK:- Listeners (aka delegate but could have several)
extension DeviceHealthAgent {
    
    // Listeners are invoked only when app is active
    func addListener(listenerId: String, handler: @escaping DeviceHealthAgentListener) {
        
        self.listeners[listenerId] = handler
        // return last measures
        handler(Array(lastMeasures.values))
    }
    
    func removeListener(listenerId: String) {
        
        self.listeners[listenerId] = nil
    }
    
    private func dispatchToListeners(measures: [Measure]) {
        
        // For each listeners ...
        for listener in listeners.values {
            listenersQueue.addOperation {
                listener(measures)
            }
        }
    }
}

// MARK:- Background mode (short term, just after app becomes in background)
extension DeviceHealthAgent {
    
    func registerBackgroundTask() {

        guard backgroundTask == .invalid else { return }

        print("registerBackgroundTask")

        // continue refresh just max during max time ...
        backgroundTask = UIApplication.shared.beginBackgroundTask { [weak self] in
            // Finalize background mode
            self?.endBackgroundTask()
        }
        
        assert(backgroundTask != .invalid)
    }
    
    func endBackgroundTask() {
        print("Background task ended.")
        if backgroundTask != .invalid {
            UIApplication.shared.endBackgroundTask(backgroundTask)
            backgroundTask = .invalid
        }
    }
}

// MARK: - Internal implementation of Models

private struct MeasureImpl: Measure {
    var date: Date?
    var instrument: Instrument
    var percentValue: Float
}

private struct NotificationImpl: Notification {
    var date: Date?
    var instrument: Instrument
    var percentValue: Float
    var typeNotification: AlarmStatus
}
