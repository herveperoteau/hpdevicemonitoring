//
//  DeviceHealthDao.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 07/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import Foundation
import CoreData

// CoreData stack

class DeviceHealthDao {
    
    struct Const {
        static let nameDB = "DeviceMonitoring"
    }
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: Const.nameDB)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    private lazy var backgroundContext: NSManagedObjectContext = {
        return self.persistentContainer.newBackgroundContext()
    }()
    
    private lazy var readContext: NSManagedObjectContext = {
        return self.persistentContainer.viewContext
    }()
}

extension DeviceHealthDao : PersistenceService {
    
    func saveMeasures(_ measures: [Measure]) throws {
        
        measures.forEach {
            let record = RecordMeasure(context: self.backgroundContext)
            record.fillWith(model: $0)
        }
        
        do {
            try self.backgroundContext.save()
        } catch {
            throw error
        }
    }
    
    func getMeasuresHistory(instrument: Instrument, limit: Int = 100) throws -> [Measure] {
        
        let fetchRequest = NSFetchRequest<RecordMeasure>(entityName: "RecordMeasure")

        // Max items
        fetchRequest.fetchLimit = limit

        // Filter
        let predicate = NSPredicate(format: "instrumentValue = %d", instrument.rawValue)
        fetchRequest.predicate = predicate

        // Sort
        let sort = NSSortDescriptor(key: "date", ascending: false)
        fetchRequest.sortDescriptors = [sort]
        
        do {
            let entities = try self.readContext.fetch(fetchRequest)
            return entities
        } catch {
            throw error
        }
    }
    
    func saveNotifications(_ notifs: [Notification]) throws {
        notifs.forEach {
            let record = RecordNotification(context: self.backgroundContext)
            record.fillWith(model: $0)
        }
        
        do {
            try self.backgroundContext.save()
        } catch {
            throw error
        }
    }
    
    func getNotificationsHistory(limit: Int = 100) throws -> [Notification] {
        
        let fetchRequest = NSFetchRequest<RecordNotification>(entityName: "RecordNotification")
        
        fetchRequest.fetchLimit = limit
        let sort = NSSortDescriptor(key: "date", ascending: false)
        fetchRequest.sortDescriptors = [sort]
        
        do {
            let entities = try self.readContext.fetch(fetchRequest)
            return entities
        } catch {
            throw error
        }
    }
    
    func deleteRecords(before: Date) throws {
        
        let datePredicate = NSPredicate(format: "date < %@", before as NSDate)
        
        let oldNotifsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "RecordNotification")
        oldNotifsRequest.predicate = datePredicate

        let oldMeasuresRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "RecordMeasure")
        oldMeasuresRequest.predicate = datePredicate

        let deleteNotifsRequest = NSBatchDeleteRequest(fetchRequest: oldNotifsRequest)
        let deleteMeasuresRequest = NSBatchDeleteRequest(fetchRequest: oldMeasuresRequest)

        do {
            try self.backgroundContext.execute(deleteNotifsRequest)
            try self.backgroundContext.execute(deleteMeasuresRequest)
            try self.backgroundContext.save()
        } catch {
            throw error
        }
    }
}
