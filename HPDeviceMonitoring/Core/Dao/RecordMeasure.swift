//
//  RecordMeasure.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 07/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import Foundation
import CoreData

class RecordMeasure: NSManagedObject {
}

extension RecordMeasure: Measure {
    
    var instrument: Instrument {
        get {
            return Instrument(rawValue: self.instrumentValue)!
        }
        set {
            self.instrumentValue = newValue.rawValue
        }
    }
    
    func fillWith(model: Measure) {
        
        self.date = model.date
        self.instrument = model.instrument
        self.percentValue = model.percentValue
    }
}
