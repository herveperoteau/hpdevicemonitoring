//
//  RecordNotification.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 07/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import Foundation
import CoreData

class RecordNotification: NSManagedObject {
}

extension RecordNotification: Notification {
    
    var instrument: Instrument {
        get {
            return Instrument(rawValue: self.instrumentValue)!
        }
        set {
            self.instrumentValue = newValue.rawValue
        }
    }

    var typeNotification: AlarmStatus {
        get {
            return AlarmStatus(rawValue: self.typeNotificationValue)!
        }
        set {
            self.typeNotificationValue = newValue.rawValue
        }
    }
    
    func fillWith(model: Notification) {
        self.date = model.date
        self.instrument = model.instrument
        self.percentValue = model.percentValue
        self.typeNotification = model.typeNotification
        self.textNotification = model.textNotification
    }
}
