//
//  MeasureModel.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 05/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import Foundation

enum Instrument: Int32, CaseIterable, CustomStringConvertible {

    case battery
    case ram
    case cpu

    var description: String {
        switch self {
        case .battery: return "Battery"
        case .ram: return "Ram"
        case .cpu: return "Cpu"
        }
    }
}

enum AlarmStatus: Int32, CustomStringConvertible {
    
    case alarm
    case recovered
    
    var description: String {
        switch self {
        case .alarm: return "Alarm"
        case .recovered: return "Recovered"
        }
    }
}

protocol Measure {
    var date: Date? { get }
    var instrument: Instrument { get }
    // Values is between 0 and 100
    var percentValue: Float { get }
}

protocol Notification: Measure {
    var typeNotification: AlarmStatus { get }
    var textNotification: String { get }
}

extension Notification {
    var textNotification: String {
        return "\(self.typeNotification.description) - \(self.instrument) : \(self.percentValue) % at \(self.date?.description ?? ""))"
    }
}

