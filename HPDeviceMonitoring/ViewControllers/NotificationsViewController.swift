//
//  AlertsViewController.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 05/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var items: [Notification] = []

    deinit {
        print("\(self.classForCoder).deinit")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView(frame: .zero)
        
        tableView.register(UINib(nibName: MeasureInfoCellIdentifier.cellNib, bundle: nil),
                           forCellReuseIdentifier: MeasureInfoCellIdentifier.cellIdentifier)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchDatas()
    }
    
    private func fetchDatas() {
        
        do {
            self.items = try ApplicationManager.shared.persistenceService.getNotificationsHistory(limit: 100)
            self.title = "Last \(self.items.count) notifications"
            self.tableView.reloadData()
            if self.items.count > 0 {
                self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
        } catch {
            print("Error getNotificationsHistory !!!")
        }
    }
}

extension NotificationsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MeasureInfoCellIdentifier.cellIdentifier, for: indexPath) as! MeasureInfoCell

        let notif = items[indexPath.row]
        cell.dateTimeLabel.text = notif.date?.description ?? ""
        cell.measureTypeLabel.text = notif.instrument.description
        cell.measureInformationLabel.text = "\(notif.textNotification)"
        
        return cell
    }
}
