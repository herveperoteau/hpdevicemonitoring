//
//  MeasuresViewController.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 05/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import UIKit

class MeasuresViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    private var items: [Measure] = []
    private var currentInstrument: Instrument = Instrument.battery
    
    deinit {
        print("\(self.classForCoder).deinit")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView(frame: .zero)
        
        tableView.register(UINib(nibName: MeasureInfoCellIdentifier.cellNib, bundle: nil),
                           forCellReuseIdentifier: MeasureInfoCellIdentifier.cellIdentifier)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchDatas(instrument: currentInstrument)
    }
    
    @IBAction func segmentedChanged() {
        print(segmentedControl.selectedSegmentIndex)
        guard let instrument = Instrument(rawValue: Int32(segmentedControl!.selectedSegmentIndex)) else {
            return
        }
        
        self.currentInstrument = instrument
        fetchDatas(instrument: self.currentInstrument)
    }
    
    private func fetchDatas(instrument: Instrument) {
        
        do {
            self.items = try ApplicationManager.shared.persistenceService.getMeasuresHistory(instrument: instrument, limit: 250)
            self.title = "Last \(self.items.count) measures"
            self.tableView.reloadData()
            if self.items.count > 0 {
                self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            }
        } catch {
            print("Error getMeasuresHistory !!!")
        }
    }
}

extension MeasuresViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MeasureInfoCellIdentifier.cellIdentifier, for: indexPath) as! MeasureInfoCell
        
        let measure = items[indexPath.row]
        cell.dateTimeLabel.text = measure.date?.description ?? ""
        cell.measureTypeLabel.text = measure.instrument.description
        cell.measureInformationLabel.text = "\(measure.percentValue) %"
        
        return cell
    }
}

