//
//  DashboardViewController.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 05/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    fileprivate struct Identifier {
        static let cellDashboard = "DashboardCellIdentifier"
        static let listenerId = "DashboardViewController"
    }

    @IBOutlet weak var tableView: UITableView!
    
    private weak var agent: DeviceHealthAgent?
    
    private var dataSource: [Instrument: Measure] = [:]
    
    deinit {
        print("\(self.classForCoder).deinit")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView(frame: .zero)
        
        self.agent = ApplicationManager.shared.agent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        agent?.addListener(listenerId: Identifier.listenerId) { [weak self] (measures) in
        
            guard let `self` = self else { return }
            
            for measure in measures {
                self.dataSource[measure.instrument] = measure
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        agent?.removeListener(listenerId: Identifier.listenerId)
    }
    
    // MARK: - Navigation
    
    @IBAction func settingsTapped(_ sender: Any) {
     
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl)
        }
    }
    
    @IBAction func unwindToDashboard(_ unwindSegue: UIStoryboardSegue) {
        print("Back to Dashboard ...")
    }
}

extension DashboardViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.values.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.cellDashboard, for: indexPath)
        
        let measure = Array(self.dataSource.values)[indexPath.row]
        
        let thresholdPercent = ApplicationManager.shared.settings.thresholdPercent(instrument: measure.instrument)
        let recoveredActivated = ApplicationManager.shared.settings.isNotifiedForRecovered(instrument: measure.instrument)
        
        
        cell.textLabel?.text = "\(measure.instrument.description) [th=\(Int(thresholdPercent))% recNotif:\(recoveredActivated)]"
        cell.detailTextLabel?.text = String(format: "%.1f %%", measure.percentValue)
        
        return cell
    }
}


