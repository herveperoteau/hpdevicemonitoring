//
//  SimulationActivityViewController.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 05/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import UIKit

class SimulationActivityViewController: UIViewController {

    @IBOutlet weak var startStopButton: UIButton!
        
    deinit {
        print("\(self.classForCoder).deinit")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateButtonStartStop()
    }
    
    private func updateButtonStartStop() {
        let agent = ApplicationManager.shared.agent
        self.startStopButton.setTitle(agent.simulFakeActivity ? "STOP" : "START", for: .normal)
    }
    
    @IBAction func startStopActivityGenerator() {
        print("startStopActivityGenerator ...")
        let agent = ApplicationManager.shared.agent
        agent.simulFakeActivity = !agent.simulFakeActivity
        updateButtonStartStop()
    }
}
