//
//  NotificationCell.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 05/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import UIKit

struct MeasureInfoCellIdentifier {
    static let cellNib = "MeasureInfoCell"
    static let cellIdentifier = "MeasureInfoCellIdentifier"
}

// For quick implementation, these Cells are used for Notifications and Measures screen
class MeasureInfoCell: UITableViewCell {
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var measureTypeLabel: UILabel!
    @IBOutlet weak var measureInformationLabel: UILabel!
}
