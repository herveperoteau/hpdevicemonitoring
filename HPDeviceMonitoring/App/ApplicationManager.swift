//
//  ApplicationManager.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 06/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import Foundation
import UserNotifications
import UIKit

// Contains agent and persistenceService used by application, ViewControllers.
class ApplicationManager: NSObject {
    
    static let shared = ApplicationManager()
    
    let persistenceService: PersistenceService
    let agent: DeviceHealthAgent
    let settings: DeviceHealthSettings
    
    override init() {
        settings = DeviceHealthBundleSettings()
        persistenceService = DeviceHealthDao()
        agent = DeviceHealthAgent(settings: settings, persistenceService: persistenceService)
    }
    
    func didFinishLaunching() {
        
        // Clean database (keep record max 24 hours)
        let datePurgeBefore = Date(timeInterval: -86400, since: Date())
        do {
            try self.persistenceService.deleteRecords(before: datePurgeBefore)
        } catch {
            print("Error deleteRecords: \(error)")
        }
        
        
        // Start Agent
        agent.start()
        
        // Push Notification Authorization
        let options: UNAuthorizationOptions = [.alert, .sound, .badge];
        UNUserNotificationCenter.current().requestAuthorization(options: options) {
            (granted, error) in
            if !granted {
                print("Something went wrong")
            }
        }
                        
        // For Foreground notifications
        UNUserNotificationCenter.current().delegate = self
        
        // Background fetch stuff
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
        addSomeNetworkActivityToAllowBackgroundFetchMechanism()
    }
    
    func performBackgroundFetch(completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        print("performBackgroundFetch ...")
        
        agent.refreshMeasureBattery()

        addSomeNetworkActivityToAllowBackgroundFetchMechanism {
            completionHandler(.newData)
        }
    }
    
    // TODO : background fetch doesn't work for now... I don't know why...
    // I would like to use background fetch to perform long term background monitoring...
    private func addSomeNetworkActivityToAllowBackgroundFetchMechanism(completion: (()->())? = nil) {

        let dataUrl = "https://www.datadoghq.com";
        
        guard let url = URL(string: dataUrl) else { return }

        let task = URLSession.shared.dataTask(with: url) { _, _, _ in
            completion?()
        }
        
        task.resume()
    }
}

extension ApplicationManager: UNUserNotificationCenterDelegate {

    // To show notifications when app is in Foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
}
