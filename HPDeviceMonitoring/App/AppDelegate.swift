//
//  AppDelegate.swift
//  HPDeviceMonitoring
//
//  Created by Herve Peroteau on 05/08/2019.
//  Copyright © 2019 Herve Peroteau. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let fileManager = FileManager.default
        let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        print(url)
        
        ApplicationManager.shared.didFinishLaunching()
        
        return true
    }
    
    // Background fetch to continue some monitoring (Battery)
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
                
        ApplicationManager.shared.performBackgroundFetch(completionHandler: completionHandler)
    }
}

